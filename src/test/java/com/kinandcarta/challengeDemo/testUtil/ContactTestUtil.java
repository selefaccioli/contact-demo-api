package com.kinandcarta.challengeDemo.testUtil;

import com.kinandcarta.challengeDemo.entity.Address;
import com.kinandcarta.challengeDemo.entity.Contact;
import com.kinandcarta.challengeDemo.entity.PhoneNumber;
import java.util.Arrays;
import java.util.List;

public class ContactTestUtil {

    private static final String name = "Name Test";
    private static final String email = "emailtest@gmail.com";
    private static final String companyName = "Company Name Test";
    private static final PhoneNumber phoneNumber = new PhoneNumber("work", "3428765433");
    private static final List<PhoneNumber> phoneNumbers = Arrays.asList(phoneNumber);
    private static final Address address = new Address("Argentina", "Santa Fe", "Rosario", "9 de Julio", "336", "A", 2000);


    public static Contact createModelObject(Contact model) {
        model.setName(name);
        model.setEmail(email);
        model.setCompanyName(companyName);
        model.setPhoneNumbers(phoneNumbers);
        model.setAddress(address);

        return model;
    }
}
