package com.kinandcarta.challengeDemo;

import com.kinandcarta.challengeDemo.controller.ContactController;
import com.kinandcarta.challengeDemo.entity.Contact;
import com.kinandcarta.challengeDemo.repository.ContactRepository;
import com.kinandcarta.challengeDemo.testUtil.ContactTestUtil;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.runner.RunWith;
import org.springframework.test.web.servlet.MockMvc;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static junit.framework.Assert.assertEquals;

@RunWith(SpringRunner.class)
@WebMvcTest(ContactController.class)
public class ContactControllerUnitTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    ContactRepository contactRepository;

    private Long contactId = Long.valueOf(5);


    @Test
    public void getAllContacts() throws Exception {
        mockMvc.perform(get("/contacts"))
                .andExpect(status().isOk())
                .andExpect(content().json("[]"));

        verify(contactRepository, times(1)).findAll();
    }

    @Test
    public void findById() throws Exception{
        Contact contact = new Contact();
        ContactTestUtil.createModelObject(contact);
        contact.setId(contactId);

        when(contactRepository.findById(contactId)).thenReturn(java.util.Optional.of(contact));

        Optional<Contact> returned = contactRepository.findById(contactId);

        verify(contactRepository, times(1)).findById(contactId);
        verifyNoMoreInteractions(contactRepository);

        assertEquals(java.util.Optional.of(contact), returned);
    }


}
