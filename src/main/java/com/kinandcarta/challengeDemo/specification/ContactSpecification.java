package com.kinandcarta.challengeDemo.specification;

import com.kinandcarta.challengeDemo.dto.ContactSearchDTO;
import com.kinandcarta.challengeDemo.entity.Address;
import com.kinandcarta.challengeDemo.entity.Contact;
import com.kinandcarta.challengeDemo.entity.PhoneNumber;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

public class ContactSpecification implements Specification<Contact> {

    private ContactSearchDTO contactFilter;
    private Address addressFilter;

    public ContactSpecification(ContactSearchDTO contactFilter) {
        super();
        this.contactFilter = contactFilter;
    }

    @Override
    public Predicate toPredicate(Root<Contact> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        if (contactFilter.getEmail() != null || contactFilter.getPhoneNumber() != null) {
            String phoneNumber = contactFilter.getPhoneNumber();
            final Join<Contact, PhoneNumber> phones = root.join("phoneNumbers", JoinType.LEFT);
            return criteriaBuilder.or(
                    criteriaBuilder.equal(phones.get("phoneNumber"), phoneNumber),
                    criteriaBuilder.like(root.get("email"), "%"+contactFilter.getEmail()+"%")
            );
        } else if (contactFilter.getAddress() != null) {
            Address addressFilter = contactFilter.getAddress();
            return criteriaBuilder.or(
                    criteriaBuilder.equal(root.get("address").get("country"), addressFilter.getCountry()),
                    criteriaBuilder.equal(root.get("address").get("state"), addressFilter.getState())
            );
        }

        return criteriaBuilder.disjunction();

    }

}


