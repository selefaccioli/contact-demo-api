package com.kinandcarta.challengeDemo.exception;

public class ContactNotFoundException extends RuntimeException{

    public ContactNotFoundException(String exception) {
        super(exception);
    }
}
