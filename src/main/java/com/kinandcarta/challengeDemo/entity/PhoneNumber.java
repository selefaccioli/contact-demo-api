package com.kinandcarta.challengeDemo.entity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
public class PhoneNumber {

    public enum PhoneTypeEnum {
        work,
        personal
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="phone_number_id")
    private Long id;

    @NotBlank(message = "Type is mandatory")
    private String type;

    @Column(name="phone_number")
    @NotBlank(message = "Phone Number is mandatory")
    @Size(min=2, max=30)
    private String phoneNumber;

    public PhoneNumber() {
    }

    public PhoneNumber(String type, String phoneNumber) {
        this.type = type;
        this.phoneNumber = phoneNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
