package com.kinandcarta.challengeDemo.entity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="address_id")
    private Long id;

    @NotBlank(message = "Country is mandatory")
    @Size(min=2, max=30)
    private String country;

    @NotBlank(message = "State is mandatory")
    @Size(min=2, max=30)
    private String state;

    @NotBlank(message = "City is mandatory")
    @Size(min=2, max=30)
    private String city;

    @NotBlank(message = "Street is mandatory")
    @Size(min=2, max=30)
    private String street;

    @NotBlank(message = "Number is mandatory")
    @Size(min=2, max=30)
    private String number;

    @Size(min=1, max=4)
    private String department;

    private int zipCode;

    public Address() {
    }

    public Address(String country, String state, String city, String street, String number, String department, int zipCode) {
        this.country = country;
        this.state = state;
        this.city = city;
        this.street = street;
        this.number = number;
        this.department = department;
        this.zipCode = zipCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }
}
