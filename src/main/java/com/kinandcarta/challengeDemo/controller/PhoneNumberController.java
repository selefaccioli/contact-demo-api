package com.kinandcarta.challengeDemo.controller;

import com.kinandcarta.challengeDemo.entity.PhoneNumber;
import com.kinandcarta.challengeDemo.repository.PhoneNumberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/phoneNumbers")
public class PhoneNumberController {

    @Autowired
    private PhoneNumberRepository phoneNumberRepository;

    @GetMapping
    public ResponseEntity<List<PhoneNumber>> getAllPhoneNumbers() {
        List<PhoneNumber> phoneNumbersList = (List<PhoneNumber>) phoneNumberRepository.findAll();
        return new ResponseEntity<List<PhoneNumber>>(phoneNumbersList, HttpStatus.OK);
    }
}
