package com.kinandcarta.challengeDemo.controller;

import com.kinandcarta.challengeDemo.dto.ContactSearchDTO;
import com.kinandcarta.challengeDemo.entity.Contact;
import com.kinandcarta.challengeDemo.exception.ContactNotFoundException;
import com.kinandcarta.challengeDemo.repository.ContactRepository;
import com.kinandcarta.challengeDemo.specification.ContactSpecification;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/contacts")
public class ContactController {

    @Autowired
    private ContactRepository contactRepository;

    @GetMapping
    public ResponseEntity<List<Contact>> getAllContacts() {
        List<Contact> contactsList = (List<Contact>) contactRepository.findAll();
        return new ResponseEntity<List<Contact>>(contactsList, HttpStatus.OK);
    }


    @GetMapping
    @RequestMapping("/{id}")
    public Optional<Contact> getOne(@PathVariable Long id){
        try {
            return contactRepository.findById(id);
        } catch (ContactNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Contact Not Found");
        }

    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Contact create(@RequestBody Contact contact){
        return contactRepository.save(contact);
    }

    @RequestMapping(value="/{id}", method = RequestMethod.PUT)
    public Contact update(@PathVariable Long id, @RequestBody Contact contact) {
        Contact existingContact;
        try {
            existingContact = contactRepository.findById(id).orElse(null);
        } catch (ContactNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Contact Not Found");
        }
         BeanUtils.copyProperties(contact, existingContact, "id");
         return contactRepository.save(existingContact);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id){
        contactRepository.deleteById(id);
    }

    @GetMapping("/search")
    public ResponseEntity<List<Contact>> searchContacts(@RequestBody ContactSearchDTO searchData){
        if (searchData.getEmail() != null || searchData.getPhoneNumber() != null) {
            Specification<Contact> specification = new ContactSpecification(searchData);
            return executeSearch(specification);

        } else if (searchData.getAddress() != null) {
            Specification<Contact> specification = new ContactSpecification(searchData);
            return executeSearch(specification);
        }

       return null;
    }


    private ResponseEntity<List<Contact>> executeSearch(Specification<Contact> specification) {
        List<Contact> foundedContacts = contactRepository.findAll(specification);
        return new ResponseEntity<List<Contact>>(foundedContacts, HttpStatus.OK);
    }

}
