package com.kinandcarta.challengeDemo.repository;

import com.kinandcarta.challengeDemo.entity.PhoneNumber;
import org.springframework.data.repository.CrudRepository;

public interface PhoneNumberRepository extends CrudRepository<PhoneNumber, Long> {
}
