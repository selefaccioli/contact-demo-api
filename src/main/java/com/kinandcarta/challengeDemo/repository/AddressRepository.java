package com.kinandcarta.challengeDemo.repository;

import com.kinandcarta.challengeDemo.entity.Address;
import org.springframework.data.repository.CrudRepository;

public interface AddressRepository extends CrudRepository<Address, Long> {
}
