package com.kinandcarta.challengeDemo.dto;

import com.kinandcarta.challengeDemo.entity.Address;

public class ContactSearchDTO {

    private String email;
    private String phoneNumber;
    private Address address;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
